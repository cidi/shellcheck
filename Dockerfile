FROM alpine:3.7

ARG VERSION
RUN apk --no-cache add \
        --virtual build \
        cabal \
        ghc \
        musl-dev \
    && cabal update \
    && cabal install \
        --bindir=/usr/local/bin/ \
        "shellcheck-${VERSION}" \
    && apk del build \
    && rm -rf ~/.cabal ~/.ghc \
    && apk --no-cache add \
        libffi libgmpxx \
    && shellcheck --version \
    && [ "$(shellcheck --version | grep ^version: | awk '{ print $2 }')" = "$VERSION" ]
